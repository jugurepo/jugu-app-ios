//
//  ExploreTableViewCell.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ExploreTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var subExploreNameLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    
    static let identifier = "ExploreTableViewCell"
    
    // MARK: - View life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        let image = selected ? UIImage(named: "check-icon") : UIImage(named: "uncheck-icon")
        checkButton.setImage(image, for: .normal)
    }

}
