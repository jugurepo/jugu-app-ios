//
//  IntroduceCollectionViewCell.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class IntroduceCollectionViewCell: UICollectionViewCell {
    // MARK: - View life cycle
    @IBOutlet weak var introduceImageView: UIImageView!
    
    static let identifier = "IntroduceCollectionViewCell"
    
    // MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
