//
//  UpdateInfoTableViewCell.swift
//  jugu_chat
//
//  Created by An Phan on 1/8/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class UpdateInfoTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var infoNameLabel: UILabel!
    @IBOutlet weak var infomationLabel: UILabel!
    
    static let identifier = "UpdateInfoTableViewCell"
    
    var isFromInfoList = false
    
    // MARK: - View life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if isFromInfoList {
            self.accessoryType = selected ? .checkmark : .none
        }
        else {
            self.accessoryType = .disclosureIndicator
        }
    }
    
}
