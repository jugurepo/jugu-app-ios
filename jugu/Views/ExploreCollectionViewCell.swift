//
//  ExploreCollectionViewCell.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ExploreCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var exploreImageView: UIImageView!
    @IBOutlet weak var exploreNameLabel: UILabel!
    @IBOutlet weak var exploreCountLabel: UILabel!
    @IBOutlet weak var bottomExploreImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingExploreImageConstraint: NSLayoutConstraint!
    
    static let identifier = "ExploreCollectionViewCell"
    
    // MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        exploreCountLabel.roundify()
        exploreCountLabel.backgroundColor = UIColor.mainColor()
        exploreImageView.layer.cornerRadius = 6
    }
    
    // MARK: - Methods
    
    func shadowExploreName() {
        exploreNameLabel.layer.shadowColor = UIColor.black.cgColor
        exploreNameLabel.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        exploreNameLabel.layer.shadowOpacity = 1.0
        exploreNameLabel.layer.shadowRadius = 0.5
        exploreNameLabel.layer.masksToBounds = false
        exploreNameLabel.layer.cornerRadius = 4.0
    }
    
    func renderData(title: String) {
        exploreNameLabel.text = title
    }
}
