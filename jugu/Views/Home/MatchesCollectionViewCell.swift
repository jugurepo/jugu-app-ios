//
//  MatchesCollectionViewCell.swift
//  jugu
//
//  Created by An Phan on 1/20/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class MatchesCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    static let identifier = "MatchesCollectionViewCell"
    
    // MARK: - Variables
    var userList = [User]()
    var didSelectRowAt: ((User) -> Void)?
    
    // MARK: - View life cycles
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.bordered(withColor: UIColor(hexString: "EFEFEF"), width: 1)
        tableView.layer.cornerRadius = 18
        shadowView.layer.cornerRadius = 18
        tableView.dataSource = self
        tableView.delegate = self
        
        prepareShadow()
    }
    
    // MARK: - Private methods
    
    private func prepareShadow() {
        // Shadow and Radius
        let color = UIColor(hexString: "E4E4E4")
        shadowView.layer.shadowColor = color.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 3, height: 4)
        shadowView.layer.shadowRadius = 2
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowPath = nil
        //tableView.layer.masksToBounds = false
    }
}

// MARK: - UITableViewDataSource

extension MatchesCollectionViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MatchesTableViewCell.identifier, for: indexPath) as! MatchesTableViewCell
        let user = userList[indexPath.row]
        cell.renderData(user: user)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension MatchesCollectionViewCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = userList[indexPath.row]
        didSelectRowAt?(user)
    }
}
