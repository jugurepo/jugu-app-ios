//
//  ChatRequestCollectionViewCell.swift
//  jugu
//
//  Created by An Phan on 1/21/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ChatRequestCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    static let identifier = "ChatRequestCollectionViewCell"
    
    // MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.roundify()
        userImageView.roundify()
        containerView.bordered(withColor: UIColor.mainColor(), width: 1)
    }
    
    func renderData(user: User) {
        userImageView.image = user.image
        nameLabel.text = user.lastName
    }
}
