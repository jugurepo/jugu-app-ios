//
//  MessageInputView.swift
//  jugu
//
//  Created by An Phan on 1/21/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class MessageInputView: UIView {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var backgroundContainerView: UIView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var heightMessageInputViewConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        placeholderLabel.text = "Write message"
        backgroundContainerView.layer.cornerRadius = 8.0
        backgroundContainerView.backgroundColor = UIColor(hexString: "F0F0F0")
        textView.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        textView.delegate = self
    }
    
    // MARK: - Private methods
    
    func reloadView() {
        if textView.text.isEmpty {
            placeholderLabel.isHidden = false
            sendButton.isEnabled = false
        }
        else {
            placeholderLabel.isHidden = true
            sendButton.isEnabled = true
        }
        
        var height = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height
        
        if height > 120 {   // Max
            height = 120
        }
        if height < 34 {    // Min
            height = 34
        }
        textViewHeightConstraint.constant = height
        
        var tmpFrame = frame
        if UIDevice.current.userInterfaceIdiom == .phone, UIScreen.main.bounds.size.height >= 812 {
            tmpFrame.size.height = height + 46
        }
        else {
            tmpFrame.size.height = height + 30
        }
        
        frame = tmpFrame
        textView.reloadInputViews()
    }
    
    // MARK: - IBActions
    
    @IBAction func sendButtonAction(_ sender: Any) {
    }
}

// MARK: - UITextViewDelegate

extension MessageInputView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = UIColor.rgbColor(red: 111, green: 111, blue: 111)
        }
        
        reloadView()
    }
}

