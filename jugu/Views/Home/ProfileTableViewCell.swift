//
//  ProfileTableViewCell.swift
//  jugu
//
//  Created by An Phan on 1/21/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var profileSwitch: UISwitch!
    
    static let identifier = "ProfileTableViewCell"
    
    var switchValueChanged: ((UISwitch) -> Void)?
    
    // MARK: - View life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        switchValueChanged?(sender)
    }
}
