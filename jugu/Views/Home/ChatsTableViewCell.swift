//
//  ChatsTableViewCell.swift
//  jugu
//
//  Created by An Phan on 1/21/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ChatsTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeAgoLabel: UILabel!
    @IBOutlet weak var lastMsgLabel: UILabel!
    @IBOutlet weak var unreadCountLabel: UILabel!
    
    static let identifier = "ChatsTableViewCell"
    
    // MARK: - View life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        userImageView.roundify()
        unreadCountLabel.roundify()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func renderData(chat: Chat) {
        userNameLabel.text = chat.user.shortName()
        userImageView.image = chat.user.image
        timeAgoLabel.text = chat.lastMsgTime
        lastMsgLabel.text = chat.lastMsg
        unreadCountLabel.text = chat.unreadCount == 0 ? "" : "\(chat.unreadCount)"
        unreadCountLabel.isHidden = chat.unreadCount == 0
    }
}
