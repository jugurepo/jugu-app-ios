//
//  MatchesTableViewCell.swift
//  jugu
//
//  Created by An Phan on 1/20/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class MatchesTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    
    // MARK: - Variables
    var addButtonAction: ((UIButton) -> Void)?
    
    static let identifier = "MatchesTableViewCell"
    
    // MARK: - View life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        userImageView.roundify()
        addButton.roundify()
        addButton.bordered(withColor: UIColor.mainColor(), width: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Methods
    
    func renderData(user: User) {
        userImageView.image = user.image
        userNameLabel.text = user.fullName()
        companyNameLabel.text = "\(user.company)\n\(user.job)"
    }
    
    // MARK: - IBActions

    @IBAction func addButtonAction(_ sender: UIButton) {
        addButtonAction?(sender)
    }
}
