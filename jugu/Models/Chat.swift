//
//  Chat.swift
//  jugu
//
//  Created by An Phan on 1/22/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import Foundation

class Chat {
    var lastMsg = ""
    var unreadCount = 0
    var lastMsgTime = ""
    var user: User!
    
    init(lastMsg: String, unreadCount: Int, lastMsgTime: String, user: User) {
        self.lastMsg = lastMsg
        self.unreadCount = unreadCount
        self.lastMsgTime = lastMsgTime
        self.user = user
    }
}
