//
//  User.swift
//  jugu
//
//  Created by An Phan on 1/22/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class User {
    var firstName = ""
    var lastName = ""
    var company = ""
    var job = ""
    var education = ""
    var website = ""
    var desc = ""
    var location = ""
    var image: UIImage!
    
    init(firstName: String, lastName: String,
         company: String, job: String,
         education: String, website: String,
         desc: String, location: String, image: UIImage) {
        self.firstName = firstName
        self.lastName = lastName
        self.company = company
        self.job = job
        self.education = education
        self.website = website
        self.desc = desc
        self.image = image
        self.location = location
    }
    
    init(firstName: String, lastName: String, image: UIImage) {
        self.firstName = firstName
        self.lastName = lastName
        self.image = image
    }
    
    func fullName() -> String {
        return "\(firstName) \(lastName)"
    }
    
    func shortName() -> String {
        return "\(firstName) \(lastName.first!)."
    }
}
