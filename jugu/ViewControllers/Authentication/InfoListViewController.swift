//
//  InfoListViewController.swift
//  jugu_chat
//
//  Created by An Phan on 1/8/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class InfoListViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var doneBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    var titleString = ""
    var didSelectedRow: ((String) -> Void)?
    var ageDidSelectedRow: ((String, Int) -> Void)?
    var ageRowDefault = 0
    
    // MARK: - Private let/var
    private let genderList = ["Male", "Female"]
    private var professionalCategories: [String] = []
    private var seniorites: [String] = []
    private var ageList: [Int] = []
    private var displayArray: [Any] = []
    
    // MARK: - View life cycles

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set data
        professionalCategories = fetchSeniorites()
        seniorites = fetchSeniorites()
        
        prepareTableView()
        translucentNavigationBar()
        renderData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if titleString == "Age" {
            let ageDefault = ageRowDefault + 1
            let index = ageList.firstIndex(where: { $0 == ageDefault })
            let indexPath = IndexPath(row: index!, section: 0)
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)
        }
    }
    
    // MARK: - Private methods
    
    private func fetchProfessionalCategories() -> [String] {
        return ["Academic / educator", "Accountant / finance", "Admin / Operation", "Artisan / craftsman", "Artist / designer / architect", "Asset manager", "Banking / capital markets", "Broker / advisor", "Business development", "Certified professional", "Consultant", "Creative production", "Deal sourcer"]
    }
    
    private func fetchSeniorites() -> [String] {
        return ["CEO  / Managing Partner", "Partner", "Managing Director", "Division head", "Department head", "Director", "Vice president", "Manager", "Supervisor", "Associate", "Analyst", "Administrative", "Staff", "Temp staff", "Researcher", "Certified professional", "Advisor", "Consultant"]
    }
    
    private func renderData() {
        // Set data for ageList
        for age in stride(from: 1, through: 100, by: 1) {
            ageList.append(age)
        }
        
        // Choose data
        switch titleString {
        case "Sex":
            displayArray = genderList
            title = "Set Gender"
        case "Age":
            displayArray = ageList
            title = "Set Age"
        case "Professional":
            displayArray = professionalCategories
            title = "Set Professional"
        case "Seniority":
            displayArray = seniorites
            title = "Set Seniority"
        default:
            break
        }
    }
    
    private func prepareTableView() {
        let cell = UINib(nibName: UpdateInfoTableViewCell.identifier, bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: UpdateInfoTableViewCell.identifier)
    }
    
    // MARK: - IBActions
    
    @IBAction func cancelBarButtonAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBarButtonAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true) {
            if self.titleString == "Age" {
                self.ageDidSelectedRow?("\(self.displayArray[self.ageRowDefault])", self.ageRowDefault)
            }
            else {
                self.didSelectedRow?("\(self.displayArray[self.ageRowDefault])")
            }
        }
    }
}

// MARK: - UITableViewDataSource

extension InfoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UpdateInfoTableViewCell.identifier, for: indexPath) as! UpdateInfoTableViewCell
        cell.infoNameLabel.text = "\(displayArray[indexPath.row])"
        cell.isFromInfoList = true
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension InfoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ageRowDefault = indexPath.row
    }
}
