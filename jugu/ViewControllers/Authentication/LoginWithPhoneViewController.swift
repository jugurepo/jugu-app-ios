//
//  LoginWithPhoneViewController.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class LoginWithPhoneViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberPhoneView: UIView!
    @IBOutlet weak var countryNumberTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "J U G U"
        
        // Prepare button
        nextButton.layer.cornerRadius = 8
        numberPhoneView.bordered(withColor: UIColor.borderColor(), width: 2, radius: 8)
        
        // Translucent navigation bar
        translucentNavigationBar()
        
        // Show navigation bar
        navigationController?.isNavigationBarHidden = false
        
        prepareNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        prepareWhiteNavigationBar()
    }
    
    // MARK: - Methods
    
    @objc func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Private methods
    
    private func prepareNavigationBar() {
        self.navigationItem.leftItemsSupplementBackButton = false
        
        let backButton = UIButton()
        backButton.setImage(#imageLiteral(resourceName: "back-icon"), for: .normal)
        backButton.tintColor = UIColor.mainColor()
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        
        self.navigationItem.leftBarButtonItems = [backBarButtonItem]
    }
    
    // MARK: - IBActions
    
    @IBAction func textFieldEditingChanged(_ sender: Any) {
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

// MARK: - UITextFieldDelegate

extension LoginWithPhoneViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        
        // Limit charater of textField
        if textField == countryNumberTextField {
            return newString.length <= 4
        }
        
        return true
    }
}

