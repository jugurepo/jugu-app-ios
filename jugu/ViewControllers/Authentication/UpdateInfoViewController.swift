//
//  UpdateInfoViewController.swift
//  jugu_chat
//
//  Created by An Phan on 1/8/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class UpdateInfoViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Private let/var
    private let titles = ["Sex", "Age", "Professional", "Seniority"]
    
    var ageRowDefault = 14
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Personal Info"
        
        prepareTableView()
        prepareNavigationBar()
        styleNavigationBar()
        translucentNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Methods
    
    @objc func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func doneButtonAction() {
        performSegue(withIdentifier: "showExplorePageSegue", sender: nil)
    }
    
    // MARK: - Private methods
    
    private func prepareNavigationBar() {
        // Added right barButtonItem
        let doneBarButtonItem = UIBarButtonItem(title: "Done",
                                                style: .plain,
                                                target: self,
                                                action: #selector(doneButtonAction))
        
        navigationItem.rightBarButtonItem = doneBarButtonItem
    }
    
    private func prepareTableView() {
        let cell = UINib(nibName: UpdateInfoTableViewCell.identifier, bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: UpdateInfoTableViewCell.identifier)
    }
}

// MARK: - UITableViewDataSource

extension UpdateInfoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UpdateInfoTableViewCell.identifier, for: indexPath) as! UpdateInfoTableViewCell
        cell.infoNameLabel.text = titles[indexPath.row]
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension UpdateInfoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let infoListNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InfoListNC") as! UINavigationController
        let infoListVC = infoListNC.topViewController as! InfoListViewController
        infoListVC.titleString = titles[indexPath.row]
        infoListVC.ageRowDefault = ageRowDefault
        infoListVC.didSelectedRow = { info in
            let cell = tableView.cellForRow(at: indexPath) as! UpdateInfoTableViewCell
            cell.infomationLabel.text = info
        }
        
        infoListVC.ageDidSelectedRow = { age, index in
            let cell = tableView.cellForRow(at: indexPath) as! UpdateInfoTableViewCell
            cell.infomationLabel.text = age
            
            // Index
            self.ageRowDefault = index
        }
        
        DispatchQueue.main.async {
            self.present(infoListNC, animated: true, completion: nil)
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
