//
//  LoginViewController.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loginWithFbButton: UIButton!
    @IBOutlet weak var loginWithPhoneButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    private var images = [#imageLiteral(resourceName: "landing_first"), #imageLiteral(resourceName: "landing_second"), #imageLiteral(resourceName: "landing_third")]
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginWithFbButton.adjustsImageWhenHighlighted = false
        loginWithPhoneButton.adjustsImageWhenHighlighted = false
        
        // Set count page of pageControl.
        pageControl.numberOfPages = images.count
        
        //loginButton.setTitle("Login with Facebook", for: .normal)
        
        prepareButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide navigation bar
        navigationController?.isNavigationBarHidden = true
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updatePageControl()
    }
    
    // MARK: - Private methods
    
    private func prepareButton() {
        loginWithFbButton.bordered(withColor: UIColor.borderColor(), width: 2, radius: 8)
        loginWithPhoneButton.layer.cornerRadius = 8
    }
    
    private func updatePageControl() {
        var index = 0
        let unSelectedDotImage = UIImage(named: "unselected-dot")
        let selectedDotImage = UIImage(named: "selected-dot")
        for dot in pageControl.subviews {
            if let imageView = pageControl.imageForSubview(dot) {
                imageView.image = index == pageControl.currentPage ? selectedDotImage : unSelectedDotImage
                index += 1
            } else {
                var dotImage = UIImage(named: "unselected-dot")
                if index == pageControl.currentPage {
                    dotImage = UIImage(named: "selected-dot")
                }
                dot.clipsToBounds = false
                dot.addSubview(UIImageView(image:dotImage))
                index += 1
            }
        }
    }
}

// MARK: - UICollectionViewDataSource

extension LoginViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IntroduceCollectionViewCell.identifier, for: indexPath) as! IntroduceCollectionViewCell
        cell.introduceImageView.image = images[indexPath.row]
        
        return cell
    }
}

// MARK: - UICollectionViewDelegate

extension LoginViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        updatePageControl()
    }
}
