//
//  VerificationViewController.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var number1TextField: MyTextField!
    @IBOutlet weak var number2TextField: MyTextField!
    @IBOutlet weak var number3TextField: MyTextField!
    @IBOutlet weak var number4TextField: MyTextField!
    @IBOutlet weak var number5TextField: MyTextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var tryReceiveCodeButton: UIButton!
    
    // MARK: - Variables
    var codeString = ""
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "J U G U"
        nextButton.layer.cornerRadius = 8
        nextButton.activated(false)
        number1TextField.becomeFirstResponder()
        
        prepareTextField()
        prepareNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        styleNavigationBar()
    }
    
    // MARK: - Methods
    
    @objc func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Private methods
    
    private func prepareNavigationBar() {
        self.navigationItem.leftItemsSupplementBackButton = false
        
        let backButton = UIButton()
        backButton.setImage(#imageLiteral(resourceName: "back-icon"), for: .normal)
        backButton.tintColor = UIColor.mainColor()
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        
        self.navigationItem.leftBarButtonItems = [backBarButtonItem]
    }
    
    private func prepareTextField() {
        number1TextField.bordered(withColor: UIColor.borderColor(), width: 2, radius: 8)
        number1TextField.myDelegate = self
        number2TextField.bordered(withColor: UIColor.borderColor(), width: 2, radius: 8)
        number2TextField.myDelegate = self
        number3TextField.bordered(withColor: UIColor.borderColor(), width: 2, radius: 8)
        number3TextField.myDelegate = self
        number4TextField.bordered(withColor: UIColor.borderColor(), width: 2, radius: 8)
        number4TextField.myDelegate = self
        number5TextField.bordered(withColor: UIColor.borderColor(), width: 2, radius: 8)
        number5TextField.myDelegate = self
    }
    
    // MARK: - IBActions
    
    @IBAction func nextButtonAction(_ button: UIButton) {
        print("codeString == \(codeString)")
    }
    
    @IBAction func tryReceiveCodeButtonAction(_ button: UIButton) {
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        if let codeText = textField.text {
            let textCount = codeText.count
            
            switch textField {
            case number1TextField:
                // Update code
                codeString.append(codeText)
                
                // Updated UI of textField
                if textCount == 1 {
                    textField.layer.borderColor = UIColor.mainColor().cgColor
                    number2TextField.isUserInteractionEnabled = true
                    number2TextField.becomeFirstResponder()
                }
            case number2TextField:
                // Update code
                codeString.append(codeText)
                
                // Updated UI of textField
                if textCount == 1 {
                    textField.layer.borderColor = UIColor.mainColor().cgColor
                    number3TextField.isUserInteractionEnabled = true
                    number3TextField.becomeFirstResponder()
                }
            case number3TextField:
                // Update code
                codeString.append(codeText)
                
                // Updated UI of textField
                if textCount == 1 {
                    textField.layer.borderColor = UIColor.mainColor().cgColor
                    number4TextField.isUserInteractionEnabled = true
                    number4TextField.becomeFirstResponder()
                }
            case number4TextField:
                // Update code
                codeString.append(codeText)
                
                // Updated UI of textField
                if textCount == 1 {
                    textField.layer.borderColor = UIColor.mainColor().cgColor
                    number5TextField.isUserInteractionEnabled = true
                    number5TextField.becomeFirstResponder()
                }
            case number5TextField:
                // Update code
                codeString.append(codeText)
                
                // Updated UI of textField
                if textCount == 1 {
                    textField.layer.borderColor = UIColor.mainColor().cgColor
                    number5TextField.resignFirstResponder()
                }
            default:
                break
            }
            
            // Enable/ Disable `Next` button
            nextButton.activated(codeString.count == 5)
        }
    }
}

// MARK: - UITextFieldDelegate

extension VerificationViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length <= 1 {
            return true
        }
        
        if newString.length > 1 {
            switch textField {
            case number1TextField:
                number2TextField.isUserInteractionEnabled = true
                number2TextField.becomeFirstResponder()
            case number2TextField:
                number3TextField.isUserInteractionEnabled = true
                number3TextField.becomeFirstResponder()
            case number3TextField:
                number4TextField.isUserInteractionEnabled = true
                number4TextField.becomeFirstResponder()
            case number4TextField:
                number5TextField.isUserInteractionEnabled = true
                number5TextField.becomeFirstResponder()
            default:
                break
            }
            
            return false
        }
        
        return true
    }
}

// MARK: - MyTextFieldDelegate

extension VerificationViewController: MyTextFieldDelegate {
    func textFieldDidDelete() {
        if let number1Text = number1TextField.text,
            let number2Text = number2TextField.text,
            let number3Text = number3TextField.text,
            let number4Text = number4TextField.text,
            let number5Text = number5TextField.text {
            if number1Text.isEmpty {
                // Update code
                if codeString.count == 1 {
                    codeString.remove(at: codeString.startIndex)
                }
                
                // Updated UI for number1TextField
                number1TextField.layer.borderColor = UIColor.borderColor().cgColor
            }
            else if number2Text.isEmpty {
                // Update code
                if codeString.count == 2 {
                    let index = codeString.index(codeString.endIndex, offsetBy: -1)
                    codeString.remove(at: index)
                }
                
                // Updated UI for number2TextField
                number2TextField.layer.borderColor = UIColor.borderColor().cgColor
                number2TextField.isUserInteractionEnabled = false
                number1TextField.becomeFirstResponder()
            }
            else if number3Text.isEmpty {
                // Update code
                if codeString.count == 3 {
                    let index = codeString.index(codeString.endIndex, offsetBy: -1)
                    codeString.remove(at: index)
                }
                
                // Updated UI for number3TextField
                number3TextField.layer.borderColor = UIColor.borderColor().cgColor
                number3TextField.isUserInteractionEnabled = false
                number2TextField.becomeFirstResponder()
            }
            else if number4Text.isEmpty {
                // Update code
                if codeString.count == 4 {
                    let index = codeString.index(codeString.endIndex, offsetBy: -1)
                    codeString.remove(at: index)
                }
                
                // Updated UI for number4TextField
                number4TextField.layer.borderColor = UIColor.borderColor().cgColor
                number4TextField.isUserInteractionEnabled = false
                number3TextField.becomeFirstResponder()
            }
            else if number5Text.isEmpty {
                // Update code
                if codeString.count == 5 {
                    let index = codeString.index(before: codeString.endIndex)
                    codeString.remove(at: index)
                }
                
                // Updated UI for number5TextField
                number5TextField.layer.borderColor = UIColor.borderColor().cgColor
                number5TextField.isUserInteractionEnabled = false
                number4TextField.becomeFirstResponder()
            }
            
            // Enable/ Disable `Next` button
            nextButton.activated(codeString.count == 5)
        }
    }
}

