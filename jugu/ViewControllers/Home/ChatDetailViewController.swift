//
//  ChatDetailViewController.swift
//  jugu
//
//  Created by An Phan on 1/21/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ChatDetailViewController: UIViewController {
    
    // MARK: - Variables
    let messageInputView: MessageInputView = MessageInputView.fromNib()
    var chat: Chat!
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.tintColor = UIColor.mainColor()
        navigationController?.navigationBar.barTintColor = UIColor(hexString: "FAFAFA")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.avenirMedium(size: 20)]
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override var inputAccessoryView: UIView? {
        if UIDevice.current.userInterfaceIdiom == .phone, UIScreen.main.bounds.size.height >= 812 {
            messageInputView.frame.size.height = messageInputView.heightMessageInputViewConstraint.constant + 16
        }
        return messageInputView
    }
    
    // MARK: - Methods
    
    @objc func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Private methods
    
    private func prepareNavigation() {
        navigationItem.hidesBackButton = true
        
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "back-icon"), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        // Set left navigationItem
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItems = [backBarButtonItem]
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        imageView.image = chat.user.image
        let userNameLabel = UILabel()
        userNameLabel.text = chat.user.shortName()
        userNameLabel.frame.origin = CGPoint(x: 42, y: 0)
        userNameLabel.frame.size = CGSize(width: userNameLabel.intrinsicContentSize.width, height: 36)
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 42 + userNameLabel.intrinsicContentSize.width, height: 36))
        titleView.addSubview(imageView)
        titleView.addSubview(userNameLabel)

        navigationItem.titleView = titleView
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        messageInputView.textView.resignFirstResponder()
    }
}
