//
//  ChatsViewController.swift
//  jugu
//
//  Created by An Phan on 1/21/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ChatsViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Variables
    var chats = [Chat]()
    var requestUsers = [User]()
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Chats"
        
        chats = Array(fakeChats[..<3])
        requestUsers = Array(fetchNewRequests()[..<5])
        
        // prepare collectionView
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        
        // prepare tableview
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        prepareMainNavigationBar()
    }
    
    // MARK: - Methods
    
    func fetchChats() -> [Chat] {
        return fakeChats
    }
    
    func fetchNewRequests() -> [User] {
        return fakeUsers
    }
}


// MARK: - UITableViewDataSource

extension ChatsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChatsTableViewCell.identifier, for: indexPath) as! ChatsTableViewCell
        let chat = chats[indexPath.row]
        cell.renderData(chat: chat)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ChatsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatDetailVC") as! ChatDetailViewController
        let chat = chats[indexPath.row]
        chatDetailVC.chat = chat
        navigationController?.pushViewController(chatDetailVC, animated: true)
    }
}

// MARK: - UITableViewDataSource

extension ChatsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return requestUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChatRequestCollectionViewCell.identifier, for: indexPath) as! ChatRequestCollectionViewCell
        let user = requestUsers[indexPath.row]
        cell.renderData(user: user)
        
        return cell
    }
}
