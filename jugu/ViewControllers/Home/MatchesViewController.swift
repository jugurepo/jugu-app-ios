//
//  MatchesViewController.swift
//  jugu
//
//  Created by An Phan on 1/20/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit
import MSPeekCollectionViewDelegateImplementation

class MatchesViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Private let/var
    private var delegate: MSPeekCollectionViewDelegateImplementation!
    
    // MARK: - Variables
    var displayUserArray = [[User]]()
    var userArray = [User]()
    var filterUser = [User]()
    
    // MARK: - View life cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Matches"
        userArray = fetchUser()
        displayUserArray = fetchDisplayUserArray()
        
        prepareCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        prepareMainNavigationBar()
        resetNavigationBar()
    }
    
    // MARK: - Private methods
    
    private func fetchUser() -> [User] {
        return fakeUsers
    }
    
    private func fetchDisplayUserArray() -> [[User]] {
        return fakeUsers.chunked(into: 5)
    }
    
    private func prepareCollectionView() {
        collectionView.configureForPeekingDelegate()
        delegate = MSPeekCollectionViewDelegateImplementation(cellSpacing: 0,
                                                              cellPeekWidth: 32,
                                                              scrollThreshold: 32,
                                                              maximumItemsToScroll: 1,
                                                              numberOfItemsToShow: 1)
        collectionView.delegate = delegate
    }
}

// MARK: - UICollectionViewDataSource

extension MatchesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return displayUserArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MatchesCollectionViewCell.identifier, for: indexPath) as! MatchesCollectionViewCell
        let userList = displayUserArray[indexPath.row]
        cell.userList = userList
        cell.didSelectRowAt = { user in
            let matchesDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MatchesDetailVC") as! MatchesDetailViewController
            matchesDetailVC.user = user
            
            self.navigationController?.pushViewController(matchesDetailVC, animated: true)
        }
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension MatchesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

