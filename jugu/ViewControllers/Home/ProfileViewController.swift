//
//  ProfileViewController.swift
//  jugu
//
//  Created by An Phan on 1/21/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    let images = [UIImage(named: "settings"), UIImage(named: "help"), UIImage(named: "bell"), UIImage(named: "audio"), UIImage(named: "info")]
    let titles = ["General", "Help", "Notifications", "Sounds", "Privacy"]
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Profile"

        userImageView.layer.cornerRadius = 8
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        prepareMainNavigationBar()
    }
}

// MARK: - UITableViewDataSource

extension ProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.identifier, for: indexPath) as! ProfileTableViewCell
        cell.iconImageView.image = images[indexPath.row]
        cell.profileLabel.text = titles[indexPath.row]
        if indexPath.row == 2 || indexPath.row == 3 {
            cell.profileSwitch.isHidden = false
        }
        
        
        return cell
    }
}
