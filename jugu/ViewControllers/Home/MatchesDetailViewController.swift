//
//  MatchesDetailViewController.swift
//  jugu
//
//  Created by An Phan on 1/21/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit
import SafariServices

class MatchesDetailViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var heightContentViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var dotLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var universityLabel: UILabel!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var expertiseLabel: UILabel!
    @IBOutlet weak var interestLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Variables
    var user: User!
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let topScrollView = UIApplication.shared.statusBarFrame.height + 44
        scrollView.contentInset = UIEdgeInsets(top: -topScrollView, left: 0, bottom: 0, right: 0)
        dotLabel.text = "●"
        dotLabel.textColor = UIColor.mainColor()
        
        attributeText()
        translucentNavigationBar()
        prepareNavigationBar()
        prepareUI()
        renderData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width,
                                        height: descriptionLabel.frame.maxY + 20)
        heightContentViewConstraint.constant = descriptionLabel.frame.maxY + 20
    }
    
    // MARK: - Methods
    
    @objc func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Private methods
    
    private func renderData() {
        userImageView.image = user.image
        userNameLabel.text = user.fullName()
        companyNameLabel.text = user.company
        jobNameLabel.text = user.job
        companyLabel.text = user.company
        cityLabel.text = user.location
        universityLabel.text = user.education
        websiteButton.setTitle(user.website, for: .normal)
        descriptionLabel.text = user.desc
    }
    
    private func prepareNavigationBar() {
        navigationItem.hidesBackButton = true
        
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItems = [backBarButtonItem]
    }
    
    private func prepareUI() {
        userImageView.roundify()
        addButton.roundify()
        addButton.bordered(withColor: UIColor.mainColor(), width: 1)
    }
    
    private func attribute(label: UILabel, labelText: String, textRange: String) {
        label.text = labelText
        let attributedString = NSMutableAttributedString(string: label.text!)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.mainColor()]
        
        let range = (label.text! as NSString).range(of: textRange)
        attributedString.addAttributes(attributes, range: range)
        
        label.attributedText = attributedString
    }
    
    private func attributeText() {
        attribute(label: expertiseLabel, labelText: "Expertise: Graphic Designing", textRange: "Expertise:")
        attribute(label: interestLabel, labelText: "Interest: Video Games", textRange: "Interest:")
    }
    
    // MARK: - IBActions
    
    @IBAction func websiteButtonAction(_ sender: UIButton) {
        if !user.website.hasPrefix("https://") {
            if let url = "https://\(user.website)".absoluteUrl() {
                let vc = SFSafariViewController(url: url)
                present(vc, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func addButtonAction(_ sender: UIButton) {
    }
}
