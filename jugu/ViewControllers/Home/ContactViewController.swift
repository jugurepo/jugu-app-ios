//
//  ContactViewController.swift
//  jugu
//
//  Created by An Phan on 1/21/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var showSearchButton: UIButton!
    @IBOutlet weak var leadingSearchViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingSearchViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerXSearchViewConstraint: NSLayoutConstraint!
    
    // MARK: - Private let/var
    private var allContacts = [User]()
    private var filterContactsDict = [String: [User]]() {
        didSet {
            tableView.reloadData()
        }
    }
    private var allContactsDict = [String: [User]]() {
        didSet {
            tableView.reloadData()
        }
    }
    private var currentContact = 10
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Contacts"
        
        searchContainerView.roundify()
        
        // Prepare tableView
        tableView.keyboardDismissMode = .onDrag
        tableView.tableFooterView = UIView()
        tableView.addInfiniteScrolling {
            self.currentContact += 10
            delay(1, closure: {
                if self.allContacts.count < fakeContacts.count {
                    self.getContactListWithKey()
                }
                else {
                    self.tableView.infiniteScrollingView.stopAnimating()
                }
            })
        }
        
        prepareTextField()
        getContactListWithKey()
        prepareMainNavigationBar()
    }
    
    // MARK: - Private method
    
    private func getContactListWithKey() {
        
        self.tableView.infiniteScrollingView.stopAnimating()
        
        // Set default contact list
        allContacts = fetchContact(contactCount: currentContact)
        
        var groupedContactData = allContacts.chunk {
            $0.fullName().first.map { String($0) } ?? "" == $1.fullName().first.map { String($0) } ?? ""
        }
        
        // Group contacts do not follow Alphabetical
        var differentTexts: ArraySlice<User> = []
        allContacts.forEach { (contact) in
            if !contact.fullName().isAlphabetical {
                differentTexts.append(contact)
            }
        }
        
        // Add contacts do not follow Alphabetical
        if differentTexts.count != 0 {
            groupedContactData.append(differentTexts)
        }
        
        // Get contact
        groupedContactData.forEach { (contact) in
            // Get first word of title.
            var key = ""
            let firstContact = contact[contact.startIndex]
            if let firstKey = firstContact.fullName().first,
                String(firstKey).isAlphabetical {
                key = "\(firstKey)"
            }
            
            if let firstKey = firstContact.fullName().first,
                !String(firstKey).isAlphabetical {
                key = "#"
            }
            
            // Get contact by keyword
            let contactWithKey = contact.map{$0}
            allContactsDict.updateValue(contactWithKey, forKey: key)
        }
        
        filterContactsDict = allContactsDict
    }
    
    private func fetchAllContact() -> [String] {
        return ["Gordon", "Alma", "Blanche", "Ethan", "Susan", "Della", "Rosalie", "Nora", "Frances", "Leo", "Franky", "Barry", "0", "%ˆ&&", "3423423432" ]
    }

    private func prepareTextField() {
        let font = UIFont.avenirBook(size: 13)
        let placeholderColor = UIColor(hexString: "939393")
        let attributed = NSAttributedString(string: "Search for contacts", attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: placeholderColor])
        searchTextField.attributedPlaceholder = attributed
        searchTextField.textColor = .black
        searchTextField.font = font
    }
    
    private func fetchContact(contactCount: Int) -> [User] {
        let sortContacts = fakeContacts.sorted(by: { $0.fullName() < $1.fullName() })
        return Array(sortContacts[..<contactCount])
    }
    
    // MARK: - IBActions
    
    @IBAction func showSearchViewButtonAction(_ sender: UIButton) {
        centerXSearchViewConstraint.isActive = false
        leadingSearchViewConstraint.isActive = true
        trailingSearchViewConstraint.isActive = true
        showSearchButton.isHidden = true
        searchTextField.becomeFirstResponder()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        if let searchText = textField.text {
            if searchText.isEmpty {
                filterContactsDict = allContactsDict
            }
            else {
                filterContactsDict = allContactsDict.mapValues { $0.filter { $0.fullName().lowercased().contains(searchText.lowercased()) } }.filter { !$0.value.isEmpty }
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

// MARK: - UITableViewDataSource

extension ContactViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return filterContactsDict.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let contactKeys = Array(filterContactsDict.keys).sorted() { $0 < $1 }
        let keyBySection = contactKeys[section]
        if let contacts = filterContactsDict[keyBySection] {
            return contacts.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactTableViewCell.identifier, for: indexPath) as! ContactTableViewCell
        let sortedkeys = Array(filterContactsDict.keys).sorted() { $0 < $1 }
        let keyBySection = sortedkeys[indexPath.section]
        if let contacts = filterContactsDict[keyBySection] {
            let contact = contacts[indexPath.row]
            cell.userNameLabel.text = contact.fullName()
            cell.userImageView.image = contact.image
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ContactViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sortedkeys = Array(filterContactsDict.keys).sorted() { $0 < $1 }
        let keyBySection = sortedkeys[indexPath.section]
        if let contacts = filterContactsDict[keyBySection] {
            let contact = contacts[indexPath.row]
            let matchesDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MatchesDetailVC") as! MatchesDetailViewController
            matchesDetailVC.user = contact
            
            self.navigationController?.pushViewController(matchesDetailVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 36))
        headerView.backgroundColor = UIColor(hexString: "F4F4F4")
        
        // Handle contact name label
        let contactNameLabel = UILabel(frame: CGRect(x: 15,
                                                     y: 0,
                                                     width: tableView.frame.width - 30,
                                                     height: headerView.frame.height))
        let sortedkeys = Array(filterContactsDict.keys).sorted() { $0 < $1 }
        contactNameLabel.text = sortedkeys[section]
        contactNameLabel.textColor = UIColor.mainColor()
        contactNameLabel.font = UIFont.avenirMedium(size: 18)
        headerView.addSubview(contactNameLabel)
        
        return headerView
    }
}
