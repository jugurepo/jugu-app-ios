//
//  ExploreViewController.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var searchExpertiseTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var filterExpertiseCollectionView: UICollectionView!
    @IBOutlet weak var expertiseCollectionView: UICollectionView!
    @IBOutlet weak var heightFilterExpertiseConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterExpertiseContainerView: UIView!
    
    // MARK: - Private let/var
    private var leftRightSpacing: CGFloat = 13
    private var filteredExpertieses: [String] = []
    private var expertises: [String] = []
    private var filterExpertiseIndexPath: IndexPath!
    private var subExpertisesCount = 0
    private var heightFilterExperties: CGFloat = 102
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Updated UI
        searchButton.adjustsImageWhenHighlighted = false
        searchContainerView.layer.cornerRadius = 6
        expertiseCollectionView.keyboardDismissMode = .onDrag
        tableView.keyboardDismissMode = .onDrag
        heightFilterExpertiseConstraint.constant = 0
        
        // Set current data
        filteredExpertieses = fetchExpertises()
        
        // Display searchTextField
        prepareSearchText(placeHolder: "What is your expertise?")
        
        // Hide navigation bar
        navigationController?.isNavigationBarHidden = true
        
        prepareCollectionView()
    }
    
    // MARK: - Private methods
    
    private func fetchExpertises() -> [String] {
        return ["Accounting", "Advertising", "Agricultural", "Apparel & Textile", "Arts & Design", "Automotive", "Banking & Finance", "Beverage", "Chemicals", "Computer"]
    }
    
    private func prepareCollectionView() {
        let cell = UINib(nibName: ExploreCollectionViewCell.identifier, bundle: nil)
        // prepare filterExpertiseCollectionView
        filterExpertiseCollectionView.register(cell, forCellWithReuseIdentifier: ExploreCollectionViewCell.identifier)
        filterExpertiseCollectionView.contentInset = UIEdgeInsets(top: 0, left: leftRightSpacing, bottom: 0, right: leftRightSpacing)
        
        // prepare expertiseCollectionView
        expertiseCollectionView.register(cell, forCellWithReuseIdentifier: ExploreCollectionViewCell.identifier)
        expertiseCollectionView.contentInset = UIEdgeInsets(top: 9, left: 0, bottom: 0, right: 0)
    }
    
    private func prepareSearchText(placeHolder: String) {
        let color = UIColor.mainColor()
        let font = UIFont.avenirMedium(size: 14)
        let attributedString = NSAttributedString(string: placeHolder,
                                                 attributes: [NSAttributedString.Key.foregroundColor: color,
                                                              NSAttributedString.Key.font: font])
        searchExpertiseTextField.attributedPlaceholder = attributedString
        searchExpertiseTextField.textColor = color
        searchExpertiseTextField.font = font
    }
    
    // MARK: - IBActions
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        if let keyword = textField.text {
            filteredExpertieses = keyword.isEmpty ? expertises : expertises.filter({$0.lowercased().contains(keyword.lowercased())})
        }
        expertiseCollectionView.reloadData()
    }
    
    
    @IBAction func searchButtonAction(_ sender: Any) {
        searchExpertiseTextField.becomeFirstResponder()
    }
}

// MARK: - UICollectionViewDataSource

extension ExploreViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == expertiseCollectionView ? filteredExpertieses.count : expertises.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExploreCollectionViewCell.identifier, for: indexPath) as! ExploreCollectionViewCell
        // Difference collectionView
        let isExpertise = collectionView == expertiseCollectionView
        
        // Render data
        let title = isExpertise ? filteredExpertieses[indexPath.row] : expertises[indexPath.row]
        cell.renderData(title: title)
        
        // Updated UI for explore cell
        cell.leadingExploreImageConstraint.constant = isExpertise ? 0 : 3
        cell.bottomExploreImageConstraint.constant = isExpertise ? 0 : 5
        cell.exploreNameLabel.font = UIFont.avenirMedium(size: isExpertise ? 16 : 14)
        
        if isExpertise {
            cell.shadowExploreName()
            cell.exploreCountLabel.isHidden = true
        }
        else {
            cell.exploreCountLabel.isHidden = !isExpertise && subExpertisesCount == 0
            if filterExpertiseIndexPath == indexPath {
                cell.exploreImageView.bordered(withColor: UIColor.mainColor(), width: tableView.isHidden ? 0 : 2)
                cell.exploreCountLabel.text = "\(subExpertisesCount)"
                cell.exploreCountLabel.isHidden = subExpertisesCount == 0
            }
            else {
                cell.exploreImageView.layer.borderWidth = 0
            }
        }
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ExploreViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == expertiseCollectionView {
            let title = filteredExpertieses[indexPath.row]
            // Show Filter Collection View
            heightFilterExpertiseConstraint.constant = heightFilterExperties
            
            // Append data from allExperties to filterExperties
            let isExistInExplore = expertises.contains(where: { $0 == title })
            if !isExistInExplore {
                expertises.append(title)
            }
            else {
                print("Explore already exists")
            }
            
            // Reload data
            filterExpertiseCollectionView.reloadData()
            
            // Update searchTextField
            prepareSearchText(placeHolder: "Search")
        }
        else {
            if filterExpertiseIndexPath == indexPath {
                // Save indexPath current
                filterExpertiseIndexPath = nil
                
                // Hide tableView
                tableView.isHidden = true
                
                // Change background filterExpertiseContainerView
                filterExpertiseContainerView.backgroundColor = UIColor(hexString: "ECEEF3")
            }
            else {
                // Save indexPath current
                filterExpertiseIndexPath = indexPath
                
                // Show tableView
                tableView.isHidden = false
                
                // Change background filterExpertiseContainerView
                filterExpertiseContainerView.backgroundColor = .white
            }
            
            filterExpertiseCollectionView.reloadData()
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemCountForRow: CGFloat = collectionView == expertiseCollectionView ? 2 : 3
        let totalSpacingOfFilterExplore = leftRightSpacing * 4
        let totalSpacingBetweenItem: CGFloat = collectionView == expertiseCollectionView ? 10 : totalSpacingOfFilterExplore
        
        if collectionView == expertiseCollectionView {
            let width = (collectionView.frame.width - totalSpacingBetweenItem) / itemCountForRow
            let height = (width * 2) / 3
            return CGSize(width: width, height: height)
        }
        else {
            let width = (collectionView.frame.width - totalSpacingOfFilterExplore) / itemCountForRow
            let height = collectionView.frame.height
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 13
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == expertiseCollectionView ? 10 : 0
    }
}

// MARK: - UITableViewDataSource

extension ExploreViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExploreTableViewCell.identifier, for: indexPath)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ExploreViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        subExpertisesCount += 1
        filterExpertiseCollectionView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        subExpertisesCount -= 1
        filterExpertiseCollectionView.reloadData()
    }
}
