//
//  UIPageControl.swift
//  jugu_chat
//
//  Created by An Phan on 1/8/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

extension UIPageControl {
    func imageForSubview(_ view: UIView) -> UIImageView? {
        var icon: UIImageView?
        
        if let iconImageView = view as? UIImageView {
            icon = iconImageView
        } else {
            for foundView in view.subviews {
                if let imageView = foundView as? UIImageView {
                    icon = imageView
                    break
                }
            }
        }
        
        return icon
    }
}
