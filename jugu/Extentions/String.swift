//
//  String.swift
//  jugu
//
//  Created by An Phan on 1/22/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import Foundation

extension String {
    func absoluteUrl() -> URL? {
        return URL(string: self)
    }
    
    var isAlphabetical: Bool {
        return range(of: "^[a-zA-Z]", options: .regularExpression) != nil
    }
}
