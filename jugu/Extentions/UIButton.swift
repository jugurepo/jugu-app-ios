//
//  UIButton.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

extension UIButton {
    func activated(_ activated: Bool) {
        isEnabled = activated
        alpha = activated ? 1.0 : 0.5
    }
}
