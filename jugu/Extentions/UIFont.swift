//
//  UIFont.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

extension UIFont {
    
    /// useful macros
    
    class func avenirBook(size s: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Book", size: s)!
    }
    
    class func avenirLight(size s: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Light", size: s)!
    }
    
    class func avenirMedium(size s: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Medium", size: s)!
    }
    
    class func avenirBack(size s: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Black", size: s)!
    }
    
    class func avenirRoman(size s: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Roman", size: s)!
    }
    
    // Font Casanova Scotia
    
    class func casanovaScotia(size s: CGFloat) -> UIFont {
        return UIFont(name: "CasanovaScotia", size: s)!
    }
    
    // Font Crono Pro
    
    class func cronosProRegular(size s: CGFloat) -> UIFont {
        return UIFont(name: "CronosPro-Regular", size: s)!
    }
    
    class func cronosProBold(size s: CGFloat) -> UIFont {
        return UIFont(name: "CronosPro-Bold", size: s)!
    }
    
    class func cronosProLight(size s: CGFloat) -> UIFont {
        return UIFont(name: "CronosPro-Lt" , size: s)!
    }
    
    class func cronosProSemibold(size s: CGFloat) -> UIFont {
        return UIFont(name: "CronosPro-Semibold", size: s)!
    }
    
    class func cronosProCapt(size s: CGFloat) -> UIFont {
        return UIFont(name: "CronosPro-Capt", size: s)!
    }
}

