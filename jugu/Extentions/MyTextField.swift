//
//  MyTextField.swift
//  jugu_chat
//
//  Created by An Phan on 1/9/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class MyTextField: UITextField {
    
    var myDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
    
}
