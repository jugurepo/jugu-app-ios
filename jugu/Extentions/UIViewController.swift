//
//  UIViewController.swift
//  jugu_chat
//
//  Created by An Phan on 1/7/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

extension UIViewController {
    func translucentNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
    
    func resetNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func styleNavigationBar() {
        navigationController?.navigationBar.tintColor = UIColor.mainColor()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mainColor(), NSAttributedString.Key.font: UIFont.casanovaScotia(size: 26)]
    }
    
    func prepareWhiteNavigationBar() {
        navigationController?.navigationBar.tintColor = UIColor.mainColor()
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mainColor(), NSAttributedString.Key.font: UIFont.casanovaScotia(size: 26)]
    }
    
    func prepareMainNavigationBar() {
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = UIColor.mainColor()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.avenirMedium(size: 20)]
    }
}
