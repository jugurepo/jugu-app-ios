//
//  Constant.swift
//  jugu
//
//  Created by An Phan on 1/22/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

private func createFakeUsers(number: Int) -> [User] {
    return [User(firstName: "Alberto",
                 lastName: "Becker \(number)",
                 company: "ABC Company",
                 job: "Graphic Designer",
                 education: "St. Lucas University",
                 website: "www.google.com",
                 desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                 location: "New York, USA",
                 image: UIImage(named: "userAvt")!),
            User(firstName: "June",
                 lastName: "Richardson \(number)",
                 company: "ABC Company",
                 job: "UI Expert",
                 education: "St. Lucas University",
                 website: "www.albertdesigns.com",
                 desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                 location: "New York, USA",
                 image: UIImage(named: "user2")!),
            User(firstName: "Kathleen",
                 lastName: "Gill \(number)",
                 company: "ABC Company",
                 job: "UI Expert",
                 education: "St. Lucas University",
                 website: "www.albertdesigns.com",
                 desc: "Lorem ipsum dolor sit amet.",
                 location: "New York, USA",
                 image: UIImage(named: "user3")!),
            User(firstName: "Benjamin",
                 lastName: "Beck \(number)",
                 company: "ABC Company",
                 job: "Video Animator",
                 education: "St. Lucas University",
                 website: "www.albertdesigns.com",
                 desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                 location: "New York, USA",
                 image: UIImage(named: "user4")!),
            User(firstName: "Darrel",
                 lastName: "Daniels \(number)",
                 company: "ABC Company",
                 job: "Java Developer",
                 education: "St. Lucas University",
                 website: "www.albertdesigns.com",
                 desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                 location: "New York, USA",
                 image: UIImage(named: "user5")!)
    ]
}

private func informationHeros(number: Int) -> [User] {
    return [User(firstName: "Iron",
                 lastName: "Man \(number)",
                 company: "ABC Company",
                 job: "Graphic Designer",
                 education: "St. Lucas University",
                 website: "www.google.com",
                 desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                 location: "New York, USA",
                 image: UIImage(named: "userAvt")!),
            User(firstName: "Captain",
                 lastName: "America \(number)",
                 company: "ABC Company",
                 job: "UI Expert",
                 education: "St. Lucas University",
                 website: "www.albertdesigns.com",
                 desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                 location: "New York, USA",
                 image: UIImage(named: "user2")!),
            User(firstName: "Hulk",
                 lastName: "Incredible \(number)",
                 company: "ABC Company",
                 job: "UI Expert",
                 education: "St. Lucas University",
                 website: "www.albertdesigns.com",
                 desc: "Lorem ipsum dolor sit amet.",
                 location: "New York, USA",
                 image: UIImage(named: "user3")!),
            User(firstName: "Spider",
                 lastName: "Man \(number)",
                 company: "ABC Company",
                 job: "Video Animator",
                 education: "St. Lucas University",
                 website: "www.albertdesigns.com",
                 desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                 location: "New York, USA",
                 image: UIImage(named: "user4")!),
            User(firstName: "Rune king",
                 lastName: "Thor \(number)",
                 company: "ABC Company",
                 job: "Java Developer",
                 education: "St. Lucas University",
                 website: "www.albertdesigns.com",
                 desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                 location: "New York, USA",
                 image: UIImage(named: "user5")!)
    ]
}

var fakeUsers = createFakeUsers(number: 1) + createFakeUsers(number: 2) + createFakeUsers(number: 3)
var fakeContacts = createFakeUsers(number: 1) + createFakeUsers(number: 2) + createFakeUsers(number: 3) + informationHeros(number: 1) + informationHeros(number: 2) + informationHeros(number: 3)

let fakeChats = [Chat(lastMsg: "Thank you it was very nice. How …",
                 unreadCount: 3,
                 lastMsgTime: "1 hour ago",
                 user: User(firstName: "Alberto",
                            lastName: "Becker",
                            image: UIImage(named: "userAvt")!)),
             Chat(lastMsg: "Nice to meet you.",
                 unreadCount: 9,
                 lastMsgTime: "2 hours ago",
                 user: User(firstName: "June",
                         lastName: "Richardson",
                         image: UIImage(named: "user2")!)),
             Chat(lastMsg: "It was Thursday here.",
                 unreadCount: 0,
                 lastMsgTime: "1 day ago",
                 user: User(firstName: "Kathleen",
                            lastName: "Gill",
                            image: UIImage(named: "user3")!))
             ]

