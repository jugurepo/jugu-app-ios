//
//  Common.swift
//  jugu_chat
//
//  Created by An Phan on 1/8/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import Foundation

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
